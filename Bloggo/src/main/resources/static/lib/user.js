function createUser(event) {
	event.preventDefault();
	fetch(
			"/user/create", 
			{	
				method: 'POST', 
				body: new URLSearchParams($('#signInForm').serialize())
			}
		).then(
				response => {
				if (response.status === 200) {
					handleSuccess();
				} else {
					handleError(response);
				}
				}
				).catch(
						error => 
						console.log("bloop")
						);
}

function handleError(response) {
	$("#alert-container").show();
	$("#alert-container").attr("class", "alert alert-danger");
	$("#notify").text(`Error: ${response.status}`);
}

function handleSuccess() {
	$("#alert-container").show();
	$("#alert-container").attr("class", "alert alert-success");
	$("#notify").text("User successfully created.");
}