package com.qa.Bloggo.services;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.qa.Bloggo.entities.User;

public class MyUserDetails implements UserDetails {
	
	private static final long serialVersionUID = -3484755980839679471L;
	
	private User user;
	
	public MyUserDetails(User user) {
		this.user = user;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPassword() {
		return this.user.getPassword();
	}

	@Override
	public String getUsername() {
		return this.user.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return this.user.getAccountNonExpired();
	}

	@Override
	public boolean isAccountNonLocked() {
		return this.user.getAccountNonLocked();
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return this.user.getCredentialsNonExpired();
	}

	@Override
	public boolean isEnabled() {
		return this.user.getEnabled();
	}

}
