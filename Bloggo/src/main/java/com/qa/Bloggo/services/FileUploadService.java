package com.qa.Bloggo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qa.Bloggo.entities.FileUpload;
import com.qa.Bloggo.repositories.FileUploadRepository;


@Service
public class FileUploadService {

	@Autowired
	FileUploadRepository fileUploadRepository;

	// Upload the file
	public void uploadFile(FileUpload doc) {
		fileUploadRepository.saveAndFlush(doc);
	}

	public FileUpload findByFilename(String filename) {
		return fileUploadRepository.findByFilename(filename);
	}
}