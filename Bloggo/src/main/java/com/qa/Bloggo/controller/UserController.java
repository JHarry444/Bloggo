package com.qa.Bloggo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.qa.Bloggo.entities.User;
import com.qa.Bloggo.repositories.UserRepository;
import com.qa.Bloggo.services.UserService;

@CrossOrigin
@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;

	
	@RequestMapping(value="/create", method=RequestMethod.POST)
	public User createUser(String username, String password) {
		return userService.registerNewUser(username, password);
	}
}
