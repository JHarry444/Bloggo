package com.qa.Bloggo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.qa.Bloggo.entities.User;

public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String username);
}