package com.qa.Bloggo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.qa.Bloggo.entities.FileUpload;

public interface FileUploadRepository extends JpaRepository<FileUpload, Long> {
	FileUpload findByFilename(String filename);
}