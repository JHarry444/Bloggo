package com.qa.Bloggo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BloggoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BloggoApplication.class, args);
	}
}
